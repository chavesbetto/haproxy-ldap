# HAPROXY-LDAP

## Descrição do Projeto
<p align="center">Projeto para criacao de HAPROXY como LoadBalance LDAP para ADs(Active Directory) utilizando container</p>

<img src="https://img.shields.io/static/v1?label=haproxy&message=2.3.7&color=green&style=for-the-badge&logo=ghost"/>
<img src="https://img.shields.io/static/v1?label=licence&message=MIT&color=green&style=for-the-badge&logo=opensourceinitiative.svg"/>

<p align="center">
 <a href="#Pre-requisitos">Pre-requisitos</a> • 
 <a href="#Instalacao">Instalacao</a> • 
 <a href="#Execucao">Execucao</a> • 
 <a href="#Testes">Testes</a> • 
 <a href="#Tecnologias">Tecnologias</a> • 
 <a href="#Autor">Autor</a>
</p>

### Pre-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Docker](https://www.docker.com/). 
Além disto é bom ter um editor para trabalhar como [Vim](https://www.vim.org/), [VSCode](https://code.visualstudio.com/) ou [Atom](https://atom.io/).

### Instalacao

```bash

# Instale o Docker
$ curl -fsSL https://get.docker.com | bash

# Inicialize o servico docker
$ systemctl start docker

# Habilite o servico docker na inicializacao
$ systemctl enable docker

#Instale o ldap-utils
$ apt install ldap-utils
$ yum install ldap-utils


```

### Execucao

```bash

# Clone este repositorio
$ git clone <https://gitlab.com/chavesbetto/haproxy-ldap.git>

# Acesse a pasta do projeto
$ cd haproxy/app

# Edite o arquivo haproxy.cfg com a sua preferencia
$ vim haproxy.cfg

# Build a image a partir do Dockerfile
$ docker build -t haproxy-ldap:v1 .

# Inicialize o container com a imagem buildada
$ docker run --rm -d --name=haproxy-ldap -p 389:389 haproxy-ldap:v1


```

### Testes

```bash

# Teste de consulta Ldap

$ ldapsearch -LLL -H ldap://ldap.example.com -x -D "cn=search-user,ou=People,dc=example,dc=com" -b "dc=example,dc=com" -W -s sub 'sAMAccountName=test-user' 


```
Will find "test-user" by:

- -D - Use bind user "search-user"
- -W - Prompt for password
- -H - URL of LDAP server. Non-SSL in this case; use "ldaps://" for SSL
- -b - The search base
- -s - Search scope - i.e. base for base of tree, one for on level down and sub for recursively searching down the tree (can take a while)
- Finally the search filter as a non-option argument. In this case we will search for the uid of "test-user"

### Tecnologias

As seguintes ferramentas foram utilizadas para esse projeto:

- [Haproxy](https://www.haproxy.com/)
- [Docker](https://www.docker.com/)

### Autor
---

 <img styl543109e="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/42034596?s=400&u=daa6bb8af8dfe08f055f1bea7caf4baae658e1e2&v=4" width="100px;" alt=""/>
 <br />
 <b>Beto Chaves</b>


Entre em contato!!

[![Linkedin Badge](https://img.shields.io/badge/-Beto-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/chavesbetto/)](https://www.linkedin.com/in/chavesbetto/) 
[![Gmail Badge](https://img.shields.io/badge/-chavesbetto@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:chavesbetto@gmail.com)](mailto:chavesbetto@gmail.com)
